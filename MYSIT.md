# 实战使用 (网站证书)

## Server签名私钥

```shell
openssl genrsa -out my-sit.com.key 2048
```

## CA请求文件

```shell
openssl req -new -key my-sit.com.key -out my-sit.com.csr 
----------------------
Country Name (2 letter code) [AU]:CN
State or Province Name (full name) [Some-State]:CN
Locality Name (eg, city) []:Beijing
Organization Name (eg, company) [Internet Widgits Pty Ltd]:my sit
Organizational Unit Name (eg, section) []:test
Common Name (e.g. server FQDN or YOUR name) []:www.my-site.com
Email Address []:site@qq.com

Please enter the following 'extra' attributes
to be sent with your certificate request
A challenge password []:
An optional company name []:
-------------------------
╰─± openssl req -text -noout -verify -in  my-sit.com.csr 
verify OK
Certificate Request:
    Data:
        Version: 1 (0x0)
        Subject: C = CN, ST = CN, L = Beijing, O = my sit, OU = test, CN = www.my-site.com, emailAddress = site@qq.com
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                RSA Public-Key: (2048 bit)
```

## Server生成网站证书文件

> CA机构对请求文件进行签名，随即产生证书。(my-sit.crt: 网站的证书)

```shell
openssl x509 -req -in my-sit.com.csr -CA ca.crt -CAkey ca.key -set_serial 01 -out my-sit.crt -days 365
------------------
```

![img.png](assets/img-2.png)

## 使用证书

![img.png](assets/img-3.png)





