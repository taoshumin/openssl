# openssl 生成自签名证书

- [bilibili 视频](https://www.bilibili.com/video/BV17F411g7ir?spm_id_from=333.880.my_history.page.click)

![img.png](assets/img.png)

## 一、CA 相关

### CA 创建私钥

- des3: 加密算法

```shell
openssl genrsa -des3 -out ca.key 2048
---------------
Generating RSA private key, 2048 bit long modulus (2 primes)
........................................................................................+++++
.+++++
e is 65537 (0x010001)
Enter pass phrase for ca.key: 111111
（输入密码:111111）
```

### CA 签名产生证书

> - req : 证书的格式
> - crt : 证书 `certicertificate`
> - days: 证书时间

- **产生根证书:**

```shell
openssl req -x509 -key ca.key -out ca.crt -days 365
-------------------
╰─± openssl req -x509 -key ca.key -out ca.crt -days 365
Enter pass phrase for ca.key:
（输入私钥密码：111111）
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:CN
State or Province Name (full name) [Some-State]:CN
Locality Name (eg, city) []:Beijing
Organization Name (eg, company) [Internet Widgits Pty Ltd]:test
Organizational Unit Name (eg, section) []:test
Common Name (e.g. server FQDN or YOUR name) []:www.workpiece.com  (注释：服务器域名)
Email Address []:test@qq.com
-------------------
```

- 查看CA证书信息

> - text: 文本格式

```shell
openssl x509 -in ca.crt -text
-----------------
╰─± openssl x509 -in ca.crt -text     
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            1a:f0:7a:f3:38:6e:c7:c2:90:f0:eb:1f:e7:46:7e:72:17:df:8d:8a
        Signature Algorithm: sha256W
```